﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kadulsa.Administrator.Startup))]
namespace Kadulsa.Administrator
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
